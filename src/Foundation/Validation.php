<?php

namespace aqsat_integration_kyc\absher\Foundation;



use aqsat_integration_kyc\absher\Rules\CheckPhoneRule;
use aqsat_integration_kyc\absher\Rules\IdentityNumberRule;

class Validation {

    final static public function kycForm() {

        $data = [];

        $data['identity_id'] = [
            'validation' => 'required',
            'label' => trans('absher::response.identity_id'),
            'name' => 'identity_id',
            'order' => 1,
            'type' => 'text' //todo add as constant
        ];

        $data['phone'] = [
            'validation' => 'required',
            'label' => trans('absher::response.phone'),
            'name' => 'phone',
            'order' => 2,
            'type' => 'text' //todo add as constant
        ];

        $data['date_of_birth'] = [
            'validation' => 'required',
            'label' => trans('absher::response.date_of_birth'),
            'name' => 'date_of_birth',
            'order' => 3,
            'type' => 'date' //todo add as constant
        ];

        return $data;

    }

    final static public function kycValidation(){

        $rules['identity_id'] = ['required' , new IdentityNumberRule()];

        $rules['phone'] = ['required' , new CheckPhoneRule()];

        $rules['date_of_birth'] = ['required' , 'date_format:Y-m-d' , 'before:today'];

        return $rules;

    }


    final static public function mappingResponse(array $data){

        return $data;

    }


}
